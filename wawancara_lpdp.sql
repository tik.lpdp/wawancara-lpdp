-- --------------------------------------------------------
-- Host:                         94.237.72.73
-- Server version:               5.7.35 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6265
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for wawancara_lpdp
CREATE DATABASE IF NOT EXISTS `wawancara_lpdp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `wawancara_lpdp`;

-- Dumping structure for table wawancara_lpdp.kategori_makanan
CREATE TABLE IF NOT EXISTS `kategori_makanan` (
  `kategori_makanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_makanan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kategori_makanan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wawancara_lpdp.kategori_makanan: ~0 rows (approximately)
/*!40000 ALTER TABLE `kategori_makanan` DISABLE KEYS */;
INSERT INTO `kategori_makanan` (`kategori_makanan_id`, `kategori_makanan`, `created_at`, `updated_at`) VALUES
	(1, 'Buah', '2021-09-12 02:32:40', '2021-09-12 02:32:40'),
	(2, 'Sayur', '2021-09-12 02:32:47', '2021-09-12 02:32:47'),
	(3, 'Makanan Pokok', '2021-09-12 02:32:54', '2021-09-12 02:32:54');
/*!40000 ALTER TABLE `kategori_makanan` ENABLE KEYS */;

-- Dumping structure for table wawancara_lpdp.makanan
CREATE TABLE IF NOT EXISTS `makanan` (
  `makanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_makanan_id` int(11) NOT NULL,
  `makanan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`makanan_id`) USING BTREE,
  KEY `FK_makanan_kategoriMakanan` (`kategori_makanan_id`),
  CONSTRAINT `FK_makanan_kategoriMakanan` FOREIGN KEY (`kategori_makanan_id`) REFERENCES `kategori_makanan` (`kategori_makanan_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wawancara_lpdp.makanan: ~0 rows (approximately)
/*!40000 ALTER TABLE `makanan` DISABLE KEYS */;
INSERT INTO `makanan` (`makanan_id`, `kategori_makanan_id`, `makanan`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Jeruk', '2021-09-12 02:33:08', '2021-09-12 02:33:08'),
	(2, 1, 'Apel', '2021-09-12 02:33:08', '2021-09-12 02:33:08'),
	(3, 2, 'Kangkung', '2021-09-12 02:33:08', '2021-09-12 02:33:08'),
	(4, 2, 'Bayam', '2021-09-12 02:33:08', '2021-09-12 02:33:08'),
	(5, 3, 'Nasi', '2021-09-12 02:33:08', '2021-09-12 02:33:08'),
	(6, 3, 'Sagu', '2021-09-12 02:33:08', '2021-09-12 02:33:08');
/*!40000 ALTER TABLE `makanan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
